import React from 'react';
import { ScrollView, Image, StyleSheet, Text, View } from 'react-native';
import { createStackNavigator, createAppContainer } from "react-navigation";


import Fist from './scr/open'
import Fuction from './scr/fuction'
import Fun from './scr/fun'
import Manu from './scr/menu'
import Gun from './scr/gun';
import E from './scr/e'
import Ky from './scr/ky'


const AppNavigator = createStackNavigator(
  {    
    Fist: Fist,
    Fuction : Fuction,
    Fun : Fun ,
    Manu: Manu,
    Gun : Gun,
    E: E ,
    Ky : Ky
  }, 
  {
    initialRouteName: "Fist"   ,
 
    
    defaultNavigationOptions: {
      
      headerStyle: {
        backgroundColor: '#2F4F4F',
      },
      
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
        fontSize:18,
        
      },
    }
    
  }
);

const AppContainer = createAppContainer(AppNavigator);

export default class App extends React.Component {
  
  
  
  render() {
    return (
      <AppContainer />
    )

  }
}


