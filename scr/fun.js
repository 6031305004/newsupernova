
import React from 'react';
import { ScrollView, Image, StyleSheet, Text, View ,
  ImageBackground
} from 'react-native';
import HeaderBar from '../components/header';
import Card from '../components/card'
import { TouchableOpacity } from 'react-native-gesture-handler';
import { BottomTabBar } from 'react-navigation';

export default class Fun extends React.Component {
  constructor(props) {
    super(props)
    this.state = { count: 0 }
  }
  onPress = () => {
    this.setState({
      count: this.state.count+1
    })
  }

  
  render() {
      return (
        <ScrollView>
        
       <View style={[styles.countContainer]}>
         <Text style={[styles.countText]}>
            { this.state.count !== 0 ? this.state.count: null}
          </Text>
        </View>

        <TouchableOpacity
         style={styles.button}
         onPress={this.onPress}
         >
         <Text> Number + 1 </Text>
       </TouchableOpacity>
        
        </ScrollView>       
     );
  }
}

const styles = StyleSheet.create({
  countContainer: {
    padding : 150,
    
    alignItems : 'center'
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 100
  },
  countText:{
    fontSize: 30 ,
    fontWeight: 'bold',
  }

})