import React from 'react';
import { FlatList, ScrollView,ImageBackground, Image, StyleSheet, Text, View } from 'react-native';
// import HeaderBar from '../components/header';
import Card from '../components/card'
import Listvio from '../components/listvio'

import { TouchableOpacity } from 'react-native-gesture-handler';
import mydata from '../data/timedoc1';


export default class Ky extends React.Component {
    static navigationOptions = {
        title: 'Cartoon'
    };

    constructor(props) {
        super(props)
        this.state = { data: mydata, loading: false }
    }


    loadData = async () => {
        const res =
            await fetch('https://kyaa-2a579.firebaseio.com/.json')

        const netdata = await res.json()
        console.log(netdata)
        this.setState({ data: netdata })

    }

    async componentDidMount() {
        await this.loadData()
    }render() {
        


        return (

            <ImageBackground source= {require('./image/ima1.png')}
                      style = {{flex :1}}>
                <FlatList
                    data={this.state.data}
                    renderItem={({ item }) =>
                        <TouchableOpacity
                            onPress={() => this.props.navigation.navigate('Succeed')}>

                            <Listvio title={item.title}
                                desc={item.desc}
                                img={{ uri: item.picture }} 
                                web={{ uri: item.web }}/>
                        </TouchableOpacity>
                    }
                />


            </ImageBackground>
        );
    }
}



