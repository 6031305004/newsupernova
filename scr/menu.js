import React from 'react';
import { FlatList, ScrollView, Image, StyleSheet, Text, View } from 'react-native';
// import HeaderBar from '../components/header';
import ListView from '../components/listview'
import Card from '../components/cardlo'
import { TouchableOpacity } from 'react-native-gesture-handler';



export default class Menu extends React.Component {
    static navigationOptions = {
        title: 'Select'
    };



    render() {
        let pic1 = { uri: 'https://images8.alphacoders.com/463/thumb-1920-463862.jpg' }
        let pic2 = { uri: 'https://images5.alphacoders.com/836/thumb-1920-836186.png' }
        let pic3 = { uri: 'https://images5.alphacoders.com/691/thumb-1920-691456.jpg' }

        return (
            <ScrollView>
              <TouchableOpacity
                 onPress={() => this.props.navigation.navigate('Gun')}>
              <Card img={pic1} title='To Aru Kagaku No Railgun 1-6 (TH)' />
               </TouchableOpacity>
               
                <TouchableOpacity
                 onPress={() => this.props.navigation.navigate('E')}>
              <Card img={pic2} title='Youkoso Jitsuryoku Shijou Shugi no Kyoushitsu e 1-12 (Sup)(End)' />
               </TouchableOpacity>

               <TouchableOpacity
                 onPress={() => this.props.navigation.navigate('Ky')}>
              <Card img={pic3} title='Kyoukai no Kanata  1-12 (TH)(End)' />
               </TouchableOpacity>




            </ScrollView>
           
        );
    }
}


