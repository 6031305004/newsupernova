import React from 'react';
import { Image, StyleSheet, Text, View , WebView} from 'react-native';
// import HeaderBar from './components/header';


export default class Listvio extends React.Component {
  render() {
 

    return (

        <View style={{flex:1, flexDirection:'row',marginBottom:2}}>
          <View style={{flex:1}}>
              <Image source={this.props.img} style={{height:200}}/>
          </View>
          <View style={{flex:2, paddingLeft:5}}>
              <Text>{this.props.title}</Text>
              <Text>{this.props.desc}</Text>
              <WebView
               source={this.props.web} 
               style={{ marginTop: 20, height: 300 }}
               />
              <View style={{flex:1}}>
          </View>
          </View>
        </View>

    );
  }
}


