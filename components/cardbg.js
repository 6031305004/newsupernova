import React from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';


export default class Bgopen extends React.Component {
  render() {

    return (
        <View >
          <Image style={{ height: 550 , weight : 500 }}
            source={this.props.img} 
            
            />
          <Text style={{ fontSize: 18, fontWeight: 'bold', padding: 10 }}>
            {this.props.title}
          </Text>
        </View>
 
    );
  }
}


