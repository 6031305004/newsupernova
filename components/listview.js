import React from 'react';
import { Image, StyleSheet, Text, View , WebView} from 'react-native';
// import HeaderBar from './components/header';


export default class ListView extends React.Component {
  render() {
 

    return (

        <View style={{flex:1, flexDirection:'row',marginBottom:2}}>
          <View style={{flex:1}}>
              <Image source={this.props.img} style={{height:150 ,width: 150}}/>
          </View>
          <View style={{flex:1, paddingLeft:5, paddingTop: 10}}>
              <Text>{this.props.title}</Text>
              <Text>{this.props.desc}</Text>
              <View style={{flex:1}}>
              
          </View>
          </View>
        </View>

    );
  }
}


